import subprocess
import os
# mysite
subprocess.run("django-admin startproject mysite", shell=True)
nouveau_repertoire = os.path.join(os.getcwd(), 'mysite')

os.chdir(nouveau_repertoire)

repertoire_courant = os.getcwd()

templates = os.path.join(os.getcwd(), 'templates')
registration = os.path.join(os.getcwd(),'templates','registration')

os.mkdir(templates)
os.mkdir(registration)
os.chdir(os.getcwd())

    #Fonctions pour realiser le cablage de l'apllication vers le site
#pour modifer les fichiers python Mysite/urls  application/urls  application/views
def modif(chemin,texte):
    with open(chemin, 'w',encoding='utf-8') as fichier:
        fichier.write(texte)


#   Site
#la fonction modifie mysite/urls , et realise la partie du cablage (voir les #utilisateur #validaetur etc..)
def mysite() :
    #   mysite/urls.py

    chemin_script_mysite_urls = os.path.join(repertoire_courant, 'mysite', 'urls.py')
    modification_mysite_urls = """\n
# Mysite/urls.py
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView  # new

urlpatterns = [
    path("admin/", admin.site.urls),
    path("accounts/", include("accounts.urls")),  # new
    path("accounts/", include("django.contrib.auth.urls")),
    path("", TemplateView.as_view(template_name="home.html"), name="home"),  # new
]
\n"""

    modif(chemin_script_mysite_urls,modification_mysite_urls)

def templates_login():

    chemin_templates_login = os.path.join(repertoire_courant,  'templates','registration','login.html')
    texte_templates_login = """

<!-- templates/registration/login.html -->
{% extends "base.html" %}

{% block title %}Login{% endblock %}

{% block content %}
<h2>Log In</h2>
<form method="post">
  {% csrf_token %}
  {{ form.as_p }}
  <button type="submit">Log In</button>
</form>
{% endblock %}
"""
    modif(chemin_templates_login,texte_templates_login)

def templates_base():


    chemin_templates_base = os.path.join(os.getcwd(),  'templates','base.html')
    texte_templates_base = """

<!-- templates/base.html -->
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>{% block title %}Django Auth Tutorial{% endblock %}</title>
</head>
<body>
  <main>
    {% block content %}
    {% endblock %}
  </main>
</body>
</html>


"""

    modif(chemin_templates_base,texte_templates_base)


def templates_home():

    chemin_templates_home = os.path.join(os.getcwd(),  'templates','home.html')
    texte_templates_home="""

<!-- templates/home.html-->
{% extends "base.html" %}

{% block title %}Home{% endblock %}

{% block content %}
{% if user.is_authenticated %}
  Hi {{ user.username }}!
  <p><a href="{% url 'logout' %}">Log Out</a></p>


{% else %}
  <p>You are not logged in</p>
  <a href="{% url 'login' %}">Log In</a>
  <br>
  <a href="{% url 'signup' %}">Sign up</a>
{% endif %}
{% endblock %}

"""


    modif(chemin_templates_home,texte_templates_home)


def settings():
    texte_settings = """


from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/5.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-$w*fao$4tcshh%3g5*ui&-_=+l8c&s^u4#58u7^3m5r3rlrwvp'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'accounts',                                                                                 #   accounts
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'mysite.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / "templates"],                                                    #indique ou se trouve les templates
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'mysite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/5.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}


# Password validation
# https://docs.djangoproject.com/en/5.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/5.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/5.0/howto/static-files/

STATIC_URL = 'static/'

# Default primary key field type
# https://docs.djangoproject.com/en/5.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


#indique home.html
LOGIN_REDIRECT_URL = "home"
LOGOUT_REDIRECT_URL = "home"


"""

    chemin_settings= os.path.join(os.getcwd(), 'mysite','settings.py')
    modif(chemin_settings,texte_settings)





def accounts():
    # accounts
    os.chdir(nouveau_repertoire)
    subprocess.run("python manage.py startapp accounts", shell=True)

    # urls.py
    texte_accounts_urls="""

# accounts/urls.py
from django.urls import path

from .views import SignUpView


urlpatterns = [
    path("signup/", SignUpView.as_view(), name="signup"),
]

"""
    chemin_accounts_urls=os.path.join(os.getcwd(), 'accounts','urls.py')
    modif(chemin_accounts_urls,texte_accounts_urls)

    # views.py


    texte_accounts_views="""

# accounts/views.py
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic


class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"
"""
    chemin_accounts_views=os.path.join(os.getcwd(), 'accounts','views.py')

    modif(chemin_accounts_views,texte_accounts_views)


def templates_signup():

    texte_templates_registration_signup="""

<!-- templates/registration/signup.html -->
{% extends "base.html" %}

{% block title %}Sign Up{% endblock %}

{% block content %}
  <h2>Sign up</h2>
  <form method="post">
    {% csrf_token %}
    {{ form.as_p }}
    <button type="submit">Sign Up</button>
  </form>
{% endblock %}


"""
    chemin_templates_registration_signup=os.path.join(os.getcwd(), 'templates','registration','signup.html')

    modif(chemin_templates_registration_signup,texte_templates_registration_signup)



#   mysite
mysite()
#   app accounts
accounts()

#   modif mysite/settings.py
settings()


#   html de mysite
templates_home()  #page ou on choisis de login ou signup
templates_login() #page login
templates_base()  #page après connexion

#   html de accounts
templates_signup()  #page signup


#   RUN
os.chdir(nouveau_repertoire)


subprocess.run("python manage.py migrate", shell=True)
subprocess.run("python manage.py createsuperuser", shell=True)
subprocess.run("python manage.py runserver", shell=True)
























