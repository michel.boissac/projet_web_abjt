ce dossier contient :


-un script python : projet_web.py
	"python projet_web.py"

	ce script crée le site et les applications et réalise les modifications des fichiers :
		-creation de mysite
		-creation de l'application accounts
		-cablage des urls
		-creation du dossier templates (avec les fichiers htmls)
		-modification de settings : - indique ou se trouve le fichier templates, 
                            	    	    - ajoute 'accounts' dans installed_apps
                                            - ajoute LOGIN_REDIRECT_URL = "home" ; LOGOUT_REDIRECT_URL = "home"
	Le script propose de s'inscrire en tant que superuser en ligne de commande,
	puis d'accéder au site sur http://localhost:8000/
	il est également possible de créer un compte utilisateur directement sur le site.



-un dossier Mysite , contenant le site crée par ce script python :
	contenant mysite, l'application accounts ainsi que les templates.
	pour créer un conte superuser en ligne de commande :
		-python manage.py migrate
		-subprocess.run("python manage.py createsuperuser", shell=True)
	pour acceder au site internet :
		-python manage.py runserver


